import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const store = new Vuex.Store({
	//state中是需要管理的全局变量
	state:{
		nowPlayUrl:"",//当前播放音乐链接
		nowPlaySong:{},//当前播放音乐
		isPlay:false,//是否在播放
		nowPlayInfo:{
			nowDuration:0,//当前播放的时间
			duration:0,//总时间
			durationStr:"00:00",//总时间如00:34
			nowDurationStr:"00:00",//当前播放的时间如00:12
		},
		showPlayPage:false,//展示播放页面
		showPlayList:false,//展示播放列表
		showPlayCard:false,//展示播放组件
		
		nowPlaySongListId:0,
		nowPlaySongList:[],
		nowPlaySongListLimit:20,
		nowPlaySongListOffset:0,
	},
	//mutations 是操作state中变量的方法
	mutations:{
		setShowPlayList(state, is_show){
			state.showPlayList = is_show;
		},
		setShowPlayPage(state,is_show){
			state.showPlayPage = is_show;
		},
		setShowPlayCard(state,is_show){
			state.showPlayCard = is_show;
		},
		previousSong(state){
			let list = state.nowPlaySongList;
			let nowItem = state.nowPlaySong;
			let nowIndex = 0;
			for (let i=0;i<list.length;i++) {
				if (list[i].id == nowItem.id) {
					nowIndex = i;
					break;
				}
			}
			if (nowIndex - 1 < 0) {
				nowIndex = list.length - 1;
			} else {
				nowIndex-=1;
			}
			state.nowPlaySong = list[nowIndex];
			state.showPlayCard = true;
			state.nowPlayInfo = {
				nowDuration:0,//当前播放的时间
				duration:0,//总时间
				durationStr:"00:00",//总时间如00:34
				nowDurationStr:"00:00",//当前播放的时间如00:12
			}
			state.nowPlayUrl = "";
			uni.$emit('toggleSong',{msg:'previousSong'})
		},
		nextSong(state){
			let list = state.nowPlaySongList;
			let nowItem = state.nowPlaySong;
			let nowIndex = 0;
			for (let i=0;i<list.length;i++) {
				if (list[i].id == nowItem.id) {
					nowIndex = i;
					break;
				}
			}
			if (nowIndex + 1 >= list.length) {
				nowIndex = 0;
			} else {
				nowIndex+=1;
			}
			state.nowPlaySong = list[nowIndex];
			state.showPlayCard = true;
			state.nowPlayInfo = {
				nowDuration:0,//当前播放的时间
				duration:0,//总时间
				durationStr:"00:00",//总时间如00:34
				nowDurationStr:"00:00",//当前播放的时间如00:12
			}
			state.nowPlayUrl = "";
			uni.$emit('toggleSong',{msg:'nextSong'})
		},
		setNowPlayUrl(state, url){
			state.nowPlayUrl = url;
		},
		setNowPlaySong(state, songInfo){
			state.nowPlaySong = songInfo;
			state.showPlayCard = true;
			state.nowPlayInfo = {
				nowDuration:0,//当前播放的时间
				duration:0,//总时间
				durationStr:"00:00",//总时间如00:34
				nowDurationStr:"00:00",//当前播放的时间如00:12
			}
			state.nowPlayUrl = "";
			uni.$emit('toggleSong',{msg:'setNowPlaySong'})
		},
		setNowPlayInfo(state, playInfo){
			let timer = null;
			if (timer) {
				clearTimeout(timer);
			}
			timer = setTimeout(()=>{
				if (!isNaN(playInfo.duration) && parseInt(playInfo.duration)>=0) {
					state.nowPlayInfo = playInfo;
				}
			}, 1000)
		},
		setIsPlay(state, playStatus){
			state.isPlay = playStatus;
		},
		initNowPlaySongList(state, playSongListId){
			state.nowPlaySongListId = playSongListId;
			state.nowPlaySongList = [];
			state.nowPlaySongListOffset = 0;
			state.nowPlayInfo = {
				nowDuration:0,//当前播放的时间
				duration:0,//总时间
				durationStr:"00:00",//总时间如00:34
				nowDurationStr:"00:00",//当前播放的时间如00:12
			}
			state.nowPlayUrl = "";
		},
		moreNowPlaySongList(state){
			uni.$ajax({
				url: '/playlist/track/all?limit='+state.nowPlaySongListLimit+'&offset='+state.nowPlaySongListOffset+'&id='+state.nowPlaySongListId,
				type: "get",
				loading:true,
			}).then(res => {
				if (res.songs){
					let putList = [];
					res.songs.forEach(item=>{
						putList.push({
							name:item.name,
							id:item.id,
							author:item.ar[0].name,
							desc:item.al.name,
							picUrl:item.al.picUrl
						});
					})
					if (putList.length>0) {
						if (state.nowPlaySongList.length>0) {
							state.nowPlaySongList=[...state.nowPlaySongList,...putList];
						}else{
							state.nowPlaySongList = putList;
						}
					}
					if (!state.showPlayCard) {
						if (state.nowPlaySongList.length>0) {
							state.nowPlaySong = state.nowPlaySongList[0];
						}
						state.showPlayCard = true;
						state.nowPlayInfo = {
							nowDuration:0,//当前播放的时间
							duration:0,//总时间
							durationStr:"00:00",//总时间如00:34
							nowDurationStr:"00:00",//当前播放的时间如00:12
						}
						state.nowPlayUrl = "";
					}
				}
			})
		},
		addNowPlaySongListOffset(state){
			state.nowPlaySongListOffset += state.nowPlaySongListLimit;
		},
		addNowPlaySongListStart(state, songList){
			let addItem = songList[0];
			let isExit = false;
			state.nowPlaySongList.forEach(item=>{
				if (item.id == addItem.id) {
					isExit = true;
				}
			})
			if (!isExit) {
				if (state.nowPlaySongList.length>0) {
					state.nowPlaySongList = [...songList, ...state.nowPlaySongList]
				} else {
					state.nowPlaySongList = songList;
				}
			}
			if (state.nowPlaySongList.length>0) {
				state.showPlayCard = true;
				state.nowPlayInfo = {
					nowDuration:0,//当前播放的时间
					duration:0,//总时间
					durationStr:"00:00",//总时间如00:34
					nowDurationStr:"00:00",//当前播放的时间如00:12
				}
				state.nowPlayUrl = "";
			}
		},
		addNowPlaySongListEnd(state, songList){
			if (state.nowPlaySongList.length>0) {
				state.nowPlaySongList = [state.nowPlaySongList, ...songList]
			} else {
				state.nowPlaySongList = songList;
			}
		}
	}

})

export default store