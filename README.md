# 听个音乐

#### 介绍
基于uniapp开发的移动端音乐播放器,兼容H5、小程序、APP和PC端。后端接口使用`https://github.com/Binaryify/NeteaseCloudMusicApi.git`搭建。实现热门歌单、歌单大厅、热门MV、歌曲播放、歌词滚动等功能。


#### 安装教程


```
//安装
//首先你应当克隆该项目
git clone https://gitee.com/ysssees/listen-to-music.git
 
// 然后 安装项目依赖
npm install

```

### 接下来 把项目在HBuilderX编辑器打开

## 运行-可选择运行到浏览器、微信开发者工具或APP等

![运行程序](https://foruda.gitee.com/images/1697447384579720442/12ba8645_2198566.png "屏幕截图")

## 打包发布-可选择发布APP-web和小程序等

![打包发布](https://foruda.gitee.com/images/1697447489509681860/d77e82f4_2198566.png "屏幕截图")

#### 使用说明

后端接口使用https://github.com/Binaryify/NeteaseCloudMusicApi.git搭建。运行该项目后，替换到本项目请求接口的链接即可。
请求接口配置文件：`项目目录/common/w-config.js`中的`BASE_PATH`字段
![替换请求链接](https://foruda.gitee.com/images/1697447869463233281/7cb80c89_2198566.png "屏幕截图")

#### 页面展示

![页面首页](https://foruda.gitee.com/images/1697448548272620989/d8fda258_2198566.png "屏幕截图")![歌单大厅](https://foruda.gitee.com/images/1697448586860955124/dcc5bcfe_2198566.png "屏幕截图")

![歌单详情](https://foruda.gitee.com/images/1697448723506232774/7b50ccbf_2198566.png "屏幕截图")![歌曲播放页面](https://foruda.gitee.com/images/1697448755937778620/a26a2527_2198566.png "屏幕截图")

![歌词滚动页面](https://foruda.gitee.com/images/1697448789417879534/be819541_2198566.png "屏幕截图")![播放列表](https://foruda.gitee.com/images/1697448815868398264/ba7026fa_2198566.png "屏幕截图")

![搜索页面](https://foruda.gitee.com/images/1697448866704666814/58ebb78f_2198566.png "屏幕截图")![MV页面](https://foruda.gitee.com/images/1697448893774254706/4a583c13_2198566.png "屏幕截图")

- 查看DEMO：http://music.thankmm.com
- https://www.pgyer.com/QapZnv

![加好友？？](https://foruda.gitee.com/images/1697449038703665656/35d78e68_2198566.png "屏幕截图")


