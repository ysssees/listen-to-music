/**
 * @Descripttion: 呵呵
 * @Copyright: 哈哈
 * @Link: thankmm.com
 * @Contact: 15709145752@163.com
 * @Developer: 1184216345@qq.com
 * @LastEditTime: 2023年
 */
// 备注

import Vue from 'vue'

/**
 * @description 方法文件
 * @param {Object} method
 */
import '@/common/method/method.js'

let BASE_PATH = "http://localhost:3000";

/**
 * @description 全局配置文件  往外暴露的参数
 * @param {Object} config
 */
export default {

	/**
	 * @description 全局域名  主域名 接口请求域名
	 * @param {Object} PATH
	 */
	PATH: BASE_PATH,
	WSPATH: 'ws://******/wpush', //长连接

	/**
	 * @description 本地图片地址域名
	 * @param {Object} IMGPATH
	 */
	IMGPATH: BASE_PATH,

	/**
		 * @description $alert 弹框配置默认参数
		 * @param {Object}{
			 ALERT_TITLE  提示内容
		 }   ALERT_TIME   弹框消失时间
	*/
	ALERT_TITLE: '加载中',
	ALERT_TIME: 1500,
	HOMEPAGE: "/pages/index/index",
	/**
		 * @description $alert 用户存储信息配置  默认USERINFO
		 * @param {Object}{
			 USERINFO 默认存储信息
			 URL   没有登录跳转页面
		 }
	*/
	LOGIN: {
		USERINFO: 'USERINFO',
		URL: '/pages/login/login',
		NUM: 3
	},
	/**
		 * @description 上传图片配置信息
		 * @param {Object}{
			 URL  上传图片地址 不带域名
			 TYPE  上传图片参数
			 HEADER  上传图片header
			 SOURCETYPE  上传图片类型  album图库  camera 相机
		 }
	*/
	UPLADING_IMG_CONFIG: {
		URL: '/api/common/upload',
		TYPE: 'file',
		HEADER: {
			'token': uni.getStorageSync("token")
		},
		SOURCETYPE: ['album', 'camera']
	},
	/**
	 * @description 高德地图KEY配置  小程序需配置
	 * @param {Object}
	 */
	GAODE_MAP_KEY: 'b5d14a8a70335b7b9bb6b214ee0ff8dc',

	/**
	 * @description 高德地图KEY配置  根据经纬度来查位置 逆解析
	 * @param {Object}
	 */

	GAODE_WEB_KEY: '6021547b110050b72551078c17aa2e4a',

	/**
		 * @description 设置配置参数
		 * @param {Object}{
			 v 是设置的名称  t 是设置的内容
		 }
	*/
	SETDATA: function(v, t) {
		this[v] = t
	},

	STATE: {}

}

/**
 * @description 全局混入
 * @param {Object}
 */
Vue.mixin({
	data() {
		return {
			shareConfig: {
				title: "听歌音乐",
				path: "/pages/index/index",
				imageUrl: ""
			}
		}
	},
	onShareAppMessage(res) {
		let userInfo = uni.$gSyn("USERINFO");
		let {
			title,
			path,
			imageUrl
		} = this.shareConfig;
		if (userInfo) {
			path = path + "?invite_id=" + userInfo.id
		}
		return {
			title,
			path,
			imageUrl
		}
	}
})